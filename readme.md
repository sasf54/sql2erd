# Sql to ERD processer / renderer

Creates and ERD (Entity Relationship Diagram) from an sql create file. The output will be one or several asciidoc file.

It's a cli utility written in python. Processes a single sql file with create and alter statements to extract table
structure, and relationships. For help, start with -h parameter.

You can exec it with the main.py. Dependencies are listed in **requirements.txt**, which can be pulled / installed with pip3.
All requriements (dependencies) might be contained in the system python installation.

## Usage

Minimal example:
>python3 main.py ../create_short.sql ../create.adoc 

input_file output_file

>python3 main.py -h

for help


#### Standard export
It will result in a single ascidoc file. If not configured otherwise, containing all the columns, without type, 
and containing / rendering the relationships between tables.

#### Extended export
AsciiDoc support including other relative files, so they can be reused as follwing.
Will create several files:
* {}_small.erd
  * contains table definitions:
    * only ids / foreign keys
    * no column types
* {}_detailed.erd
  * contains table definitions:
    * all the columns
    * with column types
* {}_relationship.erd
  * Relationsips are contained
* {}_small.adoc
  * contains adoc header
  * choosen / custom style definitions
  * include: {}_small.erd
  * include: {}_relationship.erd
* {}_detailed.adoc
  * contains adoc header
  * choosen / custom style definitions
  * include: {}_detailed.erd
  * include: {}_relationship.erd

#### Relationship export
Will create a single file and will contain configured table definition and relations.
It has parameters, of not given defaults both to 0.
One of them should be greater than 0.
from: how backward should search for tables
to: how forward should search for tables

Will render all the start tables, and all the tables in relation range.
Will also render the relationships in between those tables.
Good for automatically render part of the whole DB for focusing different tables, for example the user and it's related tables.


### Custom styling

I found the following style attributes, that are responding on some level:

Object types:
* entity
* relationship
* title
* header

Attributes follows object type in a {} brackets, as json, divided by coma sign ",". 

Attributes in object styles:
* bgcolor: background color most object has it, accepts #RGB HEXADECIMAL codes
  * bgcolor: "#1a2b3f"
* color: Color of the text, same format as bgcolor.
* border-color: Border color, same as bgcolor.
* size: Size of the text or label displayed (in pt?)
* font: Font-Family, expects font name, that the system is supporting. (inside kroki docker instance!)
* border: Width of the borders
* cellpadding: distance for content from cell walls
* cellspacing: How far should the text from the cell walls (in pixel?). Will shift border inwards.
* cellborder: If smaller the border, than only the inner border width, if larger, than all border's width.
* text-alignment: !were not able to give valid value!

So the two together:

> entity {bgcolor: "#f8ead4", size: "20", color: "#050a30", border: "2"  , border-color: "#123123" ,cellpadding: "5"}
## Tests

Some test cases are included, with some start sql files. Start the sql2erd on the sql, with an output to the rendered directory, 
and compare the rendered and expected files. There were some modification, because the diagram was not representing the true
db state (missing foreign key markings), and a bit chaotic labels for relationships.

* kroki's own ERD [erd.adoc](test/expected/kroki_erd.adoc)
* ERD's own diagram [nfldb](test/expected/nfldb_original.adoc)
  * which needed some modification to be comparable with the results [nfldb comparable](test/expected/nfldb_comparable.adoc) 
  * Changes:
    * some data types,
    * removed per entity's color coding
    * some columns marked as foreign key (reference to other table with ID)
    * reference labels got standardised

There's source sql files for 2 types of sql family:
* Postgres (psql)
* Mysql (mariadb)

### Test cases:

> ../test/sql/kroki_erd_psql.sql ../test/rendered/kroki_erd_psql.adoc

> ../test/sql/kroki_erd_mysql.sql ../test/rendered/kroki_erd_mysql.adoc

> ../test/sql/nfldb_mysql.sql ../test/rendered/nfldb_mysql.adoc --with-attributes 

> ../test/sql/nfldb_psql.sql ../test/rendered/nfldb_psql.adoc --with-attributes 

Just diff the resulted adocs:

> diff test/expected/kroki_ard.adoc test/rendered/kroki_erd_mysql.adoc

> diff test/expected/kroki_ard.adoc test/rendered/kroki_erd_psql.adoc

> diff test/expected/nfldb_comparable.adoc test/rendered/nfl_mysql.adoc

> diff test/expected/nfldb_comparable.adoc test/rendered/nfl_psql.adoc


## Related projects:
### Kroki
Diagram render service. Can render diagrams locally, in docker as a service, as a cli utility, or as an enterprise service.
This is more like an ecosystem, with several diagram rendering capabilities. It has good documentation.
This is more like a governor service, handling and directing queries along many diagram rendering tools.
* [official website](https://kroki.io/)
* [example page](https://kroki.io/examples.html)

### Erd
Entity Relationship Diagram generator. Haskel project, aimed to render diagrams from .erd files.
This is the middleware diagram rendering tool. It's also using GraphViz for the final render level. 

Supports to render, so can be acquired from kroki:
Tested formats and worked:
* pdf
* svg
* png (transparent)

ERD formats, handled by config, but failed with dockerized kroki: 
* eps
* bmp
* jpg
* gif
* tiff
* dot
* ps
* ps2
* plain

[github project](https://github.com/BurntSushi/erd)

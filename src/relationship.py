import stringutils
class relationship():
    """
    Represents relations in structured manner.
    Used to represent foreign key and relationship by column name - table name
    user_id:column -> user:table
    """
    name = ''
    source_table = ''
    source_column = ''
    dest_table = ''
    dest_column = ''

    def __str__(self):
        """Json like helper method for debugging purposes"""
        return 'name: {},source_table: {}, source_column: {}, dest_table: {}, dest_column: {}'.format(self.name, self.source_table, self.source_column, self.dest_table, self.dest_column)

    def export(self, style = None):
        """
        Exports current entity as ERD relationship
        :param style: str: Extra string parameter, will be concatenated after the relationship declaration
            color: "#0e3811",   text color (not the line)
            size: "24",         font size
            font: "Times",      font-family
            label: "Some text", will overwrite label (middle string on the relationship line)
        :return: str: String in ERD format
        """
        if style is None:
            return '{} *--1 {} {{label: "{} : {}"}}\n'.format(stringutils.stringutils.erd_safe(self.source_table), stringutils.stringutils.erd_safe(self.dest_table), stringutils.stringutils.erd_safe(self.source_column), stringutils.stringutils.erd_safe(self.dest_column))
        else:
            return '{} *--1 {} {{label: "{} : {}", {}}}\n'.format(stringutils.stringutils.erd_safe(self.source_table), stringutils.stringutils.erd_safe(self.dest_table), stringutils.stringutils.erd_safe(self.source_column), stringutils.stringutils.erd_safe(self.dest_column), style)

    def equals(self, other):
        """Tests if two relationships are the same"""
        if  self.source_table      == other.source_table and \
                self.source_column == other.source_column and \
                self.dest_table    == other.dest_table and \
                self.dest_column   == other.dest_column :
            return True
        return False

    def is_relevant(self, keyed_tables):
        """
        Tests if source and destination tables are found in table lise
        :param keyed_tables: []: Array with table.name as key
        :return: bool: True, if found both table.
        """
        if self.source_table in keyed_tables and self.dest_table in keyed_tables:
            return True
        return False

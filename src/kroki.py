import base64
import io
import urllib.request
import zlib

"""Handles request and response from kroki server. Optional to use."""

def render_diagram(kroki_base_url, buffer: io.StringIO, output_filename):
    """
    Encodes -> zips -> http transfers diagram -> saves response to output_filename
    On error (erd syantax) will save the received error message to the file.
    :param kroki_base_url: str: Base URl of kroki server. example: http://localhost:8000/
    :param buffer: StringIO: Rendered ERD content, ready for sending to kroki.
    :param output_filename: str: Filename to write results / error to.
    :return: None
    """
    url = kroki_base_url + base64.urlsafe_b64encode(zlib.compress(bytearray(buffer.getvalue().encode('UTF-8')), 9)).decode('UTF-8')
    request = urllib.request.urlopen(url)
    content = request.read()
    output = open(output_filename, 'wb')
    output.write(content)
    output.close()

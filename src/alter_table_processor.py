from stringutils import stringutils
from relationship import relationship

class alter_table_processor():
    """
    Class to handle alter table format foreign key deffinitions
    """
    tables = []

    def __init__(self, tables):
        self.tables = tables

    def process(self, alter_statement):
        """
        Processess simple alter statement, and creates a single relationship entity (foreign key).
        :param alter_statement: str: Alter statement block.
        :return: foreign key or None
        """
        fk = relationship()
        alter_statement = alter_statement.strip()
        fk.source_table = stringutils.get_statement(alter_statement,'alter table (?:only)? ([^ \n]*)',0)
        if 'add constraint' in alter_statement and 'foreign key' in alter_statement and 'references' in alter_statement:
            statements = stringutils.get_statements(alter_statement, r'add +constraint *([^ ]*) *foreign *key *\(([^ ]*)\) *references *([^(]*)\(([^)]*)\)')
            fk.name = statements[0]
            fk.source_column = statements[1]
            fk.dest_table = statements[2]
            fk.dest_column = statements[3]
            for table in self.tables:
                if table.name == fk.source_table:
                    table.get_column_by_name(fk.source_column).is_foreign_key = True

            return fk
        if 'add constraint' in alter_statement and 'primary key' in alter_statement :
            statements = stringutils.get_statements(alter_statement, r'add +constraint *([^ ]*) *primary *key *\(([^)]*)\)')
            for table in self.tables:
                if table.name == fk.source_table:
                    for col in statements[1].split(','):
                        col = col.strip()
                        table.get_column_by_name(col).is_id = True
            return None
        return None

    def has_table_by_name(self, tables, name):
        """
        Returns table from an array of tables by name attribute.
        Returns the first table found
        :param tables: Table[]: List of tables
        :param name: str: Name to search for
        :return: Table?: Single table entity or None if not found
        """
        for table in tables:
            if table.name == name:
                return table
        return None


    def verify_foreign_keys_n_tables(self, fks: relationship, tables):
        """
        Verifies relationships, that it has valid tables and columns.
        Modifies columns, of found in relationship to  is_foreign_key=True
        :param fks: Relationship[]: Relationship list to verify them
        :param tables: Table[]: List of tables to verify against
        :return: None
        """
        unknown_tables = []
        unknown_columns = []
        for fk in fks:
            table = self.has_table_by_name(tables, fk.source_table)
            if table == None:
                unknown_tables.append(fk.source_table)
            else:
                if fk.source_column not in table.get_column_names():
                    unknown_columns.append('source: {}:{}'.format(fk.source_table, fk.source_column))
                else:
                    column = table.get_column_by_name(fk.source_column)
                    if not column.is_id and not column.is_foreign_key:
                        column.is_foreign_key = True

            table = self.has_table_by_name(tables, fk.dest_table)
            if table == None:
                unknown_tables.append(fk.dest_table)
                fks.remove(fk)
            else:
                if fk.dest_column not in table.get_column_names():
                    unknown_columns.append('destination: {}:{}'.format(fk.dest_table, fk.dest_column))
                    fks.remove(fk)
                else:
                    column = table.get_column_by_name(fk.dest_column)
                    if not column.is_id and not column.is_foreign_key:
                        column.is_foreign_key = True


        # todo: to reporting as error
        if len(unknown_tables) > 0:
            print('unknown tables:')
            print(unknown_tables)
        if len(unknown_columns) > 0:
            print('unknown columns:')
            print(unknown_columns)


    def mine_table_connections(self, tables, fks):
        """
        Walks table list, and looks for new relationships based on column name.
        Returns only new relationships, filters out duplicates
        :param tables: Table[]: List of discovered tables
        :param fks: Relationship[]: list of existing relationships
        :return: Relationship[]: newly found Relationships
        """
        keyed_tables = {}
        new_fks = []
        for table in tables:
            if '.' in table.name:
                keyed_tables[table.name[table.name.find('.')+1:]] = table
            else:
                keyed_tables[table.name] = table

        for table in tables:
            for col in table.columns:
                if col.name.endswith('_id'):
                   tab_name = col.name.replace('_id','')
                   if tab_name in keyed_tables:
                     fk = relationship()
                     fk.source_table = table.name
                     fk.source_column = col.name
                     fk.dest_table = tab_name
                     if keyed_tables[tab_name].get_column_by_name('id'):
                         fk.dest_column = 'id'
                     else:
                         if len(keyed_tables[tab_name].get_id_columns()) > 0:
                             fk.dest_column = keyed_tables[tab_name].get_id_columns()[0]
                         else:
                             if len(keyed_tables[tab_name].columns) > 0:
                                 fk.dest_column = keyed_tables[tab_name].columns[0]
                             else:
                                 fk.dest_column = ' - '
                     new_fks.append(fk)
        # removing already existing relationships
        remove_fks = []
        for nfk in new_fks:
            for ofk in fks:
                if nfk == ofk:
                    remove_fks.append(nfk)
        for fk in remove_fks:
            if fk in new_fks:
                new_fks.remove(fk)
        return new_fks
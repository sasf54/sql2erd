import stringutils
class table:
    """
    Structured representation of a table entity.
    Used to store data, and validate relation in the later states.
    """
    name = ''
    columns = []
    column_names = []
    attributes = ''
    fks = []
    temporary = False

    def __init__(self):
        self.columns = []
        self.column_names = []
        self.fks = []
        self.temporary = False

    def get_id_columns(self):
        """Returns all the columns with is_id == True"""
        ids = []
        for col in self.columns:
            if col.is_id:
                ids.append(col)
        return ids

    def get_all_columns(self):
        return self.columns

    def get_fk_columns(self):
        fk = []
        for col in self.columns:
            if col.is_foreign_key:
                fk.append(col)
        return fk

    def get_column_names(self):
        """Regenerates and returns only the columns names"""
        if len(self.columns) != len(self.column_names):
            self.column_names.clear()
            for col in self.columns:
                self.column_names.append(col.name)
        return self.column_names

    def get_column_by_name(self, name):
        """Returns column entity by its name"""
        for col in self.columns:
            if col.name == name:
                return col
        return None

    def __str__(self):
        """Print as a json like structure for debugging purposes"""
        return 'name: {},\ncolumns: \n  {},\n attributes: {}'.format(self.name, ',\n  '.join(map(str,self.columns)), self.attributes)


    def export(self, only_ids = False, with_type = False, with_attribute = False):
        """
        Will export the table definitions
        :param only_ids: bool: will only export columns that is_id
        :param with_type: bool: export column data type too
        :param with_attribute: bool: export column data with attributes
        :return: Str: String as expected in ERD for a table definition.
        """
        result = '[' + stringutils.stringutils.erd_safe(self.name) + ']\n'
        if only_ids and len(self.get_id_columns()) ==0:
            result += 'no_ids\n'
        for col in self.columns:
            if only_ids and col.is_id or not only_ids:
                result += col.export( with_type, with_attribute) + '\n'
        result += '\n'
        return result
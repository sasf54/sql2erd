class table_column:
    """Structured data representation for a database table column"""
    name: str
    type: str
    attributes = ''
    is_id = False
    is_foreign_key = False
    is_index = False
    is_unique = False

    def __str__(self):
        """Print as a json like structure for debugging purposes"""
        return 'name: {}, type: {}, attributes: {}, is_id: {}'.format(self.name, self.type, self.attributes, self.is_id)

    def export(self, with_type: bool, with_attribute = False):
        """
        Will export the column definitions
        :param with_type: bool: export column data type too
        :param with_attribute: bool: export attributes data
        :return: Str: String as expected in ERD for a table definition.
        """
        result = ''
        if self.name == 'id' or self.is_id:
            result += '*'
        if self.is_foreign_key:
            result += '+'
        result += self.name
        result_labels = []

        if self.type:
            result_labels.append(self.type)
        if self.attributes:
            result_labels.append(self.attributes)

        if with_type == True and len(result_labels) > 0:
            result += ' {{ label: "{}" }}'.format(', '.join(result_labels))
        return result


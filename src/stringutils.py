import re

class stringutils:
    """Handles some of the common string functions."""
    def get_statement(block, regexp, group_no):
        """
        Regexp searches in the given string(block), and returns the requested group.
        Or None if not found / present in regexp
        :param block: String to search in
        :param regexp: Regexp to find / validate against
        :param group_no: partial result to return
        :return: String / None
        """
        statement = re.search(regexp, block)
        if statement is not None and len(statement.groups()) > 0:
            return statement.groups()[group_no]

    def get_statements(block, regexp):
        """
        Regexp search in the given string (block), and returns all the results as array.
        Or None if not found
        :param block: String to search in
        :param regexp: Regexp to find / validate against
        :return: [String] / None
        """
        statement = re.search(regexp, block)
        if statement is not None and len(statement.groups()) > 0:
            return statement.groups()


    def erd_safe(string):
        """
        Makes table names and column names erd safe.
        erd can't handle schema.table syntax.
        :string: Table / Column name to make safe
        :return: String.
        """
        return string.replace('.','_').replace(':','_')


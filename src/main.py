import argparse
import sql_processor
'''
Main method, call this from cli. -h gives back all the help
'''

parser = argparse.ArgumentParser(description='Loads table creates and alter tables for foreign key connections, and renders an ERD\nERD: Entity Relation Diagram\nYou can use other tools like kroki to render svg / png from it.')
parser.add_argument('input_file', help='sql file to process')
parser.add_argument('output_file', help='base filename for output')
parser.add_argument('-r', '--related', help='Render tables starting from / to of related tables. If set -start-tables also have to applied', action='store_true')
parser.add_argument('-s', '--start_tables', help='Start tables to render table relations from. Needs -r / --related to work')
parser.add_argument('-f', '--from', help='Render tables from start tables. for 2: Start table -> next table -> other table', type=int, default=0)
parser.add_argument('-t', '--to', help='Render tables to start tables. for 2: other table -> previous table -> Start table', type=int, default=0)
parser.add_argument('--only-ids', help='Render table with only columns that are primary keys / ids / or foreign keys', action='store_true', default=False)
parser.add_argument('--with-type', help='Render table columns with data type', action='store_true', default=False)
parser.add_argument('--with-attributes', help='Render table columns with attributes (NOT NULL)', action='store_true', default=False)
parser.add_argument('--extended-export', help='Render with: only-ids + no with-type + no only-ids and with-type', action='store_true', default=False)
parser.add_argument('-d','--draft', help='Render in draft style (more postit like)', action='store_true',default=False)
parser.add_argument('-F','--final', help='Render in final style (more blue / authority like)', action='store_true',default=False)
parser.add_argument('-S','--style', help='Render in final style (explicit deffinition for elements (see readme.md))',default='default')
parser.add_argument('-T','--title', help='Custom title for the diagram', default='')
parser.add_argument('-R','--no-relations', help='No relations rendered / included into diagram.\n\tWill render a narow heigh map.', action='store_true', default=False)
parser.add_argument('-e', '--temporary',help='Render temporary tables', action='store_true', default=False)
parser.add_argument('-k','--kroki', help='Render via kroki. Will output_filename to write to. Not appliable for extended export.', action='store_true', default=False)
parser.add_argument('--kroki-url', help='Base kroki url. Example: -k http://localhost:8000/', default='http://localhost:8000/')
parser.add_argument('--kroki-format', help='Output file format for kroki render. Default: svg. Options: svg, png, jpeg.', default='svg')

args = parser.parse_args()

# print(args)
if args.related and (args.start_tables is None or ( args.__dict__['from'] == 0 and args.to == 0 )) :
    print(parser.format_help())
    exit(1)

sql_pr = sql_processor.sql_processor(args)
sql_pr.load_sql_file(args.input_file)

style = args.style
if args.draft:
    style = 'draft'
if args.final:
    style = 'final'
args.__setattr__('style',style)

if args.related:
    sql_pr.export_related_tables(args.output_file, 'ctv3.device', sql_pr.tables, sql_pr.fks, sql_pr.new_fks)
else:
    if args.extended_export:
        sql_pr.extended_map_export(args.output_file, sql_pr.tables, sql_pr.fks, sql_pr.new_fks)
    else:
        sql_pr.standard_map_export(args.output_file, sql_pr.tables, sql_pr.fks, sql_pr.new_fks)


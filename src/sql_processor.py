import io
import alter_table_processor
import create_table_processor
import kroki


class sql_processor:
    """
    Main processor class.
    Handles sql processing, separates create and alter sql statements.
    Also handles ERD exporting cases.
    """
    erd_adoc_header = '[erd]\n....\n'
    erd_title_style = 'title {bgcolor: "#111111", label: "%title%", size: "48",}\n\n'
    erd_draft_style = 'entity {bgcolor: "#f8ead4", size: "20", color: "#050a30", border: "2"  , border-color: "#123123", cellpadding: "2", }\nrelationship {color: "#0e3811", size: "24", }\nheader {color: "#121b61", size: "28", }\n\n'
    erd_final_style = 'entity {bgcolor: "#d8dcff", size: "20", color: "#010106", border: "2"  , border-color: "#000633", cellpadding: "7", }\nrelationship {color: "#1e0e0e", size: "24", }\nheader {color: "#251402", size: "28", }\n\n'
    fks = []
    new_fks = []
    tables = []
    args = {}

    def __init__(self, args) -> None:
        super().__init__()
        self.args = args

    def load_sql_file(self, filename):
        """
        Loads sql file and processes it.
        Needs enough memory to load whole sql file.
        :param filename: str:
        :return: None
        """
        file = open(filename, "r")
        text = file.read()
        file.close()
        self.tables = []
        self.fks = []
        self.new_fks = []
        creates = text.lower().split("create ")
        ctp = create_table_processor.create_table_processor()
        for create in creates[1:]:
            table = ctp.process('create ' + create.split(';')[0])
            self.tables.append(table)
            for fk in table.fks:
                self.fks.append(fk)

        alters = text.lower().split('alter ')
        atp = alter_table_processor.alter_table_processor(self.tables);
        _fks = []
        for alter in alters[1:]:
            relationship = atp.process('alter ' + alter.split(';')[0])
            if relationship:
                _fks.append(relationship)

        for fk in _fks:
            if fk not in self.fks:
                self.fks.append(fk)

        atp.verify_foreign_keys_n_tables(self.fks, self.tables)

        _new_fks = atp.mine_table_connections(self.tables, self.fks)

        for fk in self.new_fks:
            if fk not in _new_fks and fk not in self.new_fks and fk not in self.fks:
                self.new_fks.append(fk)

    def standard_map_export(self, base_filename, tables,  fks, new_fks):
        """
        Exports single file adoc format document
        :param base_filename: output_file to export
        :param tables: table[] entities, found in create sql
        :param fks: relationsip[] entities
        :param new_fks: relationsip[] entities, mined by
        :return: None
        """
        style = self.get_style(self.args.style, self.args.title)
        if self.args.kroki:
            output = io.StringIO()
        else:
            output = open(base_filename,'wt')
            output.write(self.erd_adoc_header)
        output.write(style)
        for table in tables:
            if self.args.temporary or not table.temporary:
                output.write(table.export(self.args.only_ids,self.args.with_type, self.args.with_attributes))
        output.write('\n\n')
        if not self.args.no_relations:
            for fk in fks:
                output.write(fk.export())
            output.write('# relations by name\n')
            for fk in new_fks:
                output.write(fk.export('color: "#3366ff"'))
        if self.args.kroki:
            if self.args.kroki_url.endswith('/'):
                url = self.args.kroki_url + 'erd/{}/'.format(self.args.kroki_format)
            else:
                url = self.args.kroki_url + '/erd/{}/'.format(self.args.kroki_format)
            kroki.render_diagram(url, output, base_filename)
        output.close()

    def extended_map_export(self, base_filename, tables,  fks, new_fks):
        """
        Will render extended export:
        _small.erd: table definitions: only ids and without type
        _detailed.erd: table definitions: all columns and with type
        _relations.erd: relationship definitions
        _small.adoc asciidoc format, ERD header, importing: _small.erd
        _small_relations.adoc asciidoc format, ERD header, importing: _small.erd + _relations.erd
        _detaild.adoc asciidoc format, ERD header, importing: _detailed.erd
        _detaild_relations.adoc asciidoc format, ERD header, importing: _detailed.erd + _relations.erd
        :param base_filename: to use as filename tempalte to generate files above
        :param tables: Table[]: list of tables found in crate sql
        :param fks: relationsip[]: relationships
        :param new_fks:  relationsip[]: relationships
        :return: None
        """
        style = self.get_style(self.args.style, self.args.title)

        # exporting
        output = open(base_filename + '_small.erd','wt')
        for table in tables:
            output.write(table.export(True,False, False))
        output.close()
        output = open(base_filename + '_detailed.erd','wt')
        for table in tables:
            if self.args.temporary or not table.temporary:
                output.write(table.export(False,True, True))
        output.close()

        output = open(base_filename + '_relations.erd','wt')
        for fk in fks:
            output.write(fk.export())
        output.write('# relations by name\n')
        for fk in new_fks:
            output.write(fk.export('color: "#3366ff"'))
        output.close()

        output = open(base_filename + '_small.adoc','wt')
        output.write(self.erd_adoc_header)
        output.write(style)

        base_relative_filename = base_filename.split('/')[-1:][0]
        output.write('include::' + base_relative_filename + '_small.erd[]')
        output.close()
        output = open(base_filename + '_small_relations.adoc','wt')
        output.write(self.erd_adoc_header)
        output.write(style)
        output.write('include::' + base_relative_filename + '_small.erd[]\ninclude::' + base_relative_filename + '_relations.erd[]')
        output.close()

        output = open(base_filename + '_detailed.adoc','wt')
        output.write(self.erd_adoc_header)
        output.write(style)
        output.write('include::' + base_relative_filename + '_detailed.erd[]')
        output.close()

        if not self.args.no_reltions:
            output = open(base_filename + '_detailed_relations.adoc','wt')
            output.write(self.erd_adoc_header)
            output.write(style)
            output.write('include::' + base_relative_filename + '_detailed.erd[]\ninclude::' + base_relative_filename + '_relations.erd[]')
        output.close()


    def get_related_tables(self, start_tables, keyed_tables, fks, new_fks):
        """
        Will get tables related from and to in given depth to start_tables list
        :param start_tables: str array of table names, separated by ',' mark
        :param keyed_tables: array of tables keyed by table name.
        :param fks: relationsip[]: relationships
        :param new_fks:  relationsip[]: relationships
        :return: Table[]
        """
        result_tables_from = []
        result_tables_to = []
        result_tables = []
        for table in start_tables:
            result_tables_from.append(table)
            result_tables_to.append(table)
        for i in range(0, self.args.__dict__['from']):
            for table in result_tables_from:
                for t in self.walk_connections_from(table, fks, new_fks):
                    if t in keyed_tables and keyed_tables[t] not in result_tables:
                        result_tables_from.append(keyed_tables[t])
        for i in range(0, self.args.to):
            for table in result_tables_to:
                for t in self.walk_connections_to(table, fks, new_fks):
                    if t in keyed_tables and keyed_tables[t] not in result_tables:
                        result_tables_to.append(keyed_tables[t])

        for table in result_tables_from:
            if table not in result_tables:
                result_tables.append(table)
        for table in result_tables_to:
            if table not in result_tables:
                result_tables.append(table)
        return result_tables

    def walk_connections_from(self, table, fks, new_fks):
        """
        Will search for tables relating to given Table
        :param table: Table to find backward relations
        :param fks: relationsip entities
        :param new_fks:  relationsip entities
        :return: Table[]: tables found as pointing to given table
        """
        results = []
        for fk in fks:
            if fk.source_table == table.name:
                results.append(fk.dest_table)
        for fk in new_fks:
            if fk.source_table == table.name:
                results.append(fk.dest_table)
        return results

    def walk_connections_to(self, table, fks, new_fks):
        """
        Will search for tables relating from given Table
        :param table: Table to find foreward relations
        :param fks: relationsip entities
        :param new_fks:  relationsip entities
        :return: Table[]: tables that current table relations pointing to
        """
        results = []
        for fk in fks:
            if fk.dest_table == table.name:
                results.append(fk.source_table)
        for fk in new_fks:
            if fk.dest_table == table.name:
                results.append(fk.source_table)
        return results

    def export_related_tables(self, export_filename,  tables_string, tables,  fks, new_fks,):
        """
        Will render related tables ERD. Will render adoc file, or given kroki-format content into the file.
        :param export_filename: str: Will create this file with ERD content
        :param tables_string: str: coma separated list of table names
        :param tables: Table[]: Tables extracted from create sql parts
        :param fks: relationsip[]: Relationships
        :param new_fks:  relationsip[]: Relationships
        :return: None
        """
        style = self.get_style(self.args.style, self.args.title)
        keyed_tables = {}
        for table in tables:
            if '.' in table.name:
                keyed_tables[table.name] = table
            else:
                keyed_tables[table.name] = table
        start_tables = []
        missing_table = []
        for table_name in tables_string.split(","):
            if table_name not in keyed_tables:
                missing_table.append(table_name)
            else:
                start_tables.append(keyed_tables[table_name.strip()])

        if len(missing_table)>0:
            print("The following start tables were not found:")
            print("\n".join(missing_table))

        graphed_tables = self.get_related_tables(start_tables, keyed_tables, fks, new_fks)
        keyed_graph_tables = {}
        for table in graphed_tables:
            keyed_graph_tables[table.name] = table

        filtered_fks = []
        for fk in fks:
            if fk.is_relevant(keyed_graph_tables):
                filtered_fks.append(fk)
        filtered_new_fks = []
        for fk in new_fks:
            if fk.is_relevant(keyed_graph_tables):
                filtered_new_fks.append(fk)

        if self.args.kroki:
            output = io.StringIO(newline='')
        else:
            output = open(export_filename,'wt')
            output.write(self.erd_adoc_header)
        output.write(style)
        for table in graphed_tables:
            if self.args.temporary or not table.temporary:
                output.write(table.export(False,True, True))

        output.write('\n')
        if not self.args.no_relations:
            for fk in filtered_fks:
                output.write(fk.export())
            output.write('\n')
            output.write('# relations by name\n')
            for fk in filtered_new_fks:
                output.write(fk.export('color: "#3366ff"'))
        if self.args.kroki:
            if self.args.kroki_url.endswith('/'):
                url = self.args.kroki_url + 'erd/{}/'.format(self.args.kroki_format)
            else:
                url = self.args.kroki_url + '/erd/{}/'.format(self.args.kroki_format)
            kroki.render_diagram(url, output, export_filename)
        output.close()

    def get_style(self, style, title):
        """
        Gets the diagram elemnts style: draft, final, default, custom
        :param style: enum of style, or given style
        :param title: Title of the diagram
        :return: str: Element style definitions
        """
        if title is not None and title != '':
            title = self.erd_title_style.replace("%title%", title)
        match style:
            case 'default':
                style = title
            case 'draft':
                style = title + self.erd_draft_style
            case 'final':
                style = title + self.erd_final_style
        return style

import stringutils
import table
import table_column
import re
from relationship import relationship

class create_table_processor():
    """
    Class to handle sql create statements.
    Only handles tables and create sql statements.
    Can handle postgres and mysql so far, might not break on other sql dialects.
    """
    current_table: table
    minimal_columndefintion_length = 7
    create_statement = ''

    def get_inner_content(self, sql_statement):
        """
        gets content of a () block
        used to reliably extract column definitions
        :sql_statement: str:
        """
        if '(' in sql_statement:
            position = statement_start = sql_statement.find('(')
            depth = 1
            while True:
                close_caret = sql_statement.find(')', max(position,statement_start+1))
                open_caret = sql_statement.find('(', max(position,statement_start+1))
                if close_caret != -1 and open_caret != -1 :
                    if close_caret < open_caret:
                        depth -= 1
                        position = close_caret+1
                        continue
                    else:
                        depth += 1
                        position = open_caret+1
                        continue
                if close_caret != -1:
                    depth -= 1
                    position = close_caret +1
                else:
                    return sql_statement[statement_start:]
                if depth == 0:
                    return sql_statement[statement_start + 1: close_caret]
                if depth <1:
                    print('Something gone wrong')
                    exit(1)
        return sql_statement


    def process(self, create_statement):
        # print(create_statement)
        if not self.verify(create_statement):
            return None
        self.create_statement = create_statement
        self.current_table = table.table()
        # print(self.current_table)
        self.current_table.name = stringutils.stringutils.get_statement(create_statement, 'create *(?:temporary)? *table +(?:if *not *exists *)? *([a-z0-9_.]*)',0)
        self.current_table.temporary = stringutils.stringutils.get_statement(create_statement, 'create *(temporary) *table +(?:if *not *exists *)? *([a-z0-9_.]*)',0) != None
        # print('"{}"'.format(self.current_table.name))
        create_statement = create_statement.replace("\n",'')
        # print(create_statement)
        column_statement = self.get_inner_content(create_statement)
        # print(column_statement)

        self._process_columns(column_statement)
        self.current_table.attributes = create_statement[create_statement.find(column_statement)+ len(column_statement) +1 :]
        # print(self.current_table)
        return self.current_table

    def _process_columns(self, columns_statement):
        cols = columns_statement.split(',')
        # fixing colum splitting
        # column definition contains ',' character in the midle:
        # "value numeric(10,5) NOT NULL,"
        i=0
        while i < len(cols):
            if i >0 and '(' in cols[i-1]  and ')' not in cols[i-1]:
                cols[i-1] += ',' + cols[i]
                cols.pop(i)
            else:
                i += 1

        for col in cols:
            col = col.strip()
            c = table_column.table_column()
            c.name = stringutils.stringutils.get_statement(col, '([^ ]*) ',0)
            c.type = stringutils.stringutils.get_statement(col, '([^ ]*) ([^ ]*) ?',1)
            attributes = stringutils.stringutils.get_statement(col, '([^ ]*) ([^ ]*) (.*)',2)
            if attributes:
                c.attributes = re.sub("primary +key","", attributes)
            if len(col) < 5:
                print("Probably sql syntax error (create table columns statement):")
                print("\t" + self.current_table.name)
                print("\t" + columns_statement)
                continue
            if c.name.startswith('id') :
                c.is_id = True
            # mysql specific exceptions:
            if 'primary' in col and 'key' in col:
                keys = stringutils.stringutils.get_statement(col, r'primary +key *\((.*?)\)',0 )
                if keys == None and c.type and c.name:
                    c.is_id = True
                    self.current_table.columns.append(c)
                    continue
                for key in keys.split(','):
                    key = key.strip()
                    if self.current_table.get_column_by_name(key) is None:
                        print("Sql syntax / validity error, invlaid primary key")
                        print(self.create_statement)
                        exit(-1)
                    self.current_table.get_column_by_name(key).is_id = True
                continue
            if 'foreign' in col and 'key' in col:
                src_column = stringutils.stringutils.get_statement(col, r'foreign +key *\((.*?)\)',0 ).strip()
                if self.current_table.get_column_by_name(src_column) is None:
                    print("Sql syntax / validity error, invlaid primary key")
                    print(self.create_statement)
                    exit(-1)
                self.current_table.get_column_by_name(src_column).is_foreign_key = True
                if 'references' in col:
                    references = stringutils.stringutils.get_statements(col, r'references *([a-z0-9_.]*) *\(([a-z0-9_.]*)\)')
                    ref_table = references[0].strip()
                    ref_column = references[1].strip()
                    fk = relationship()
                    fk.source_table = self.current_table.name
                    fk.dest_table = ref_table
                    fk.source_column = src_column
                    fk.dest_column = ref_column
                    self.current_table.fks.append(fk)
                continue
            if ' index ' in col or ' key ' in col :
                keys = stringutils.stringutils.get_statement(col, r'(?:index|key) *.* *\((.*?)\)',0 )
                # column definitions with key option
                if 'key' in col and not keys and c.name and c.type:
                    c.is_id = True
                    self.current_table.columns.append(c)
                    continue
                for key in keys.split(','):
                    key = key.strip()
                    if self.current_table.get_column_by_name(key) is None:
                        print("Sql syntax / validity error, invlaid primary key")
                        print("mentioned column name not found in table definition")
                        print(self.create_statement)
                        # exit(-1)
                        continue
                    self.current_table.get_column_by_name(key).is_id = True
                continue
            if c.name == 'constraint':
                continue
            self.current_table.columns.append(c)



    def verify(self, create_statement):
        if 'create ' not in create_statement.lower() or ' table ' not in create_statement.lower() or ')' not in create_statement:
            return False
        return True
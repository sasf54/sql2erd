create table player(
    player_id varchar not null PRIMARY KEY,
    full_name varchar null,
    team varchar not null,
    position integer not null,
    status integer not null
);

create table team(
    team_id varchar not null  PRIMARY KEY,
    city varchar not null,
    name varchar not null
);

create table game(
    gsis_id integer not null  PRIMARY KEY,
    start_time timestamp not null,
    week smallint not null,
    season_year smallint not null,
    season_type integer not null,
    finished boolean not null,
    home_team varchar not null,
    home_score smallint not null,
    away_team varchar not null,
    away_score smallint not null
);

create table drive(
    gsis_id integer not null,
    drive_id smallint not null,
    start_field integer null,
    start_time integer not null,
    end_field integer null,
    end_time integer not null,
    pos_team varchar not null,
    pos_time integer null
);

create table play(
    gsis_id integer not null,
    drive_id smallint not null,
    play_id smallint not null,
    time integer not null,
    pos_team varchar not null,
    yardline integer null,
    down smallint null,
    yards_to_go smallint null
);

create table play_player(
    gsis_id integer not null,
    drive_id smallint not null,
    play_id smallint not null,
    player_id varchar not null,
    team varchar not null
) ;

create table meta(
    version smallint null,
    season_type integer null,
    season_year smallint null,
    week smallint null
);


ALTER TABLE ONLY player
    ADD CONSTRAINT player_team FOREIGN KEY (team) REFERENCES team(team_id);

ALTER TABLE ONLY game
    ADD CONSTRAINT game_home_team FOREIGN KEY (home_team) REFERENCES team(team_id);
ALTER TABLE ONLY game
    ADD CONSTRAINT game_away_team FOREIGN KEY (away_team) REFERENCES team(team_id);

ALTER TABLE ONLY drive
    ADD CONSTRAINT drive_game FOREIGN KEY (gsis_id) REFERENCES game(gsis_id);
ALTER TABLE ONLY drive
    ADD CONSTRAINT drive_team FOREIGN KEY (pos_team) REFERENCES team(team_id);

ALTER TABLE ONLY play
    ADD CONSTRAINT play_team FOREIGN KEY (pos_team) REFERENCES team(team_id);
ALTER TABLE ONLY play
    ADD CONSTRAINT play_team FOREIGN KEY (gsis_id) REFERENCES game(gsis_id);
ALTER TABLE ONLY play
    ADD CONSTRAINT play_team FOREIGN KEY (drive_id) REFERENCES drive(drive_id);

ALTER TABLE ONLY play_player
    ADD CONSTRAINT play_player_team FOREIGN KEY (team) REFERENCES team(team_id);
ALTER TABLE ONLY play_player
    ADD CONSTRAINT play_player_game FOREIGN KEY (gsis_id) REFERENCES game(gsis_id);
ALTER TABLE ONLY play_player
    ADD CONSTRAINT play_player_drive FOREIGN KEY (drive_id) REFERENCES drive(drive_id);
ALTER TABLE ONLY play_player
    ADD CONSTRAINT play_player_play FOREIGN KEY (play_id) REFERENCES play(play_id);
ALTER TABLE ONLY play_player
    ADD CONSTRAINT play_player_player FOREIGN KEY (player_id) REFERENCES player(player_id);

ALTER TABLE ONLY drive
    ADD CONSTRAINT drive_ids PRIMARY KEY (gsis_id, drive_id) ;

ALTER TABLE ONLY play
    ADD CONSTRAINT play_ids PRIMARY KEY (gsis_id, drive_id, play_id) ;

ALTER TABLE ONLY play_player
    ADD CONSTRAINT play_player_ids PRIMARY KEY (gsis_id, drive_id, play_id, player_id) ;


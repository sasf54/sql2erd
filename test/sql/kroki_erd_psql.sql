create table Person(
    name character varying(100) not null primary key,
    height double precision,
    weight double precision,
    birth_location_id integer
);

create table Location(
    id integer AUTO INCREMENT NOT NULL,
    city character varying(100) not null,
    state character varying(100) not null,
    country character varying(100) not null
);

ALTER TABLE ONLY Person
    ADD CONSTRAINT persion_birthl_location FOREIGN KEY (birth_location_id) REFERENCES Location(id);

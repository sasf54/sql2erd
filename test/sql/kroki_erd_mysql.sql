create table if not exists Person(
    name char(100) not null,
    height double precision,
    weight double precision,
    birth_location_id integer,
    foreign key (birth_location_id) REFERENCES Location(id),
    PRIMARY KEY (name)
);

create table Location(
    id integer AUTO INCREMENT NOT NULL,
    city char(100) not null,
    state char(100) not null,
    country char(100) not null
);

create table player(
    player_id varchar not null,
    full_name varchar null,
    team varchar not null,
    position integer not null,
    status integer not null,
    foreign key (team) REFERENCES team(team_id),
    PRIMARY KEY (player_id)
);

create table team(
    team_id varchar not null,
    city varchar not null,
    name varchar not null,
    PRIMARY KEY (team_id)
);

create table game(
    gsis_id integer not null,
    start_time timestamp not null,
    week smallint not null,
    season_year smallint not null,
    season_type integer not null,
    finished boolean not null,
    home_team varchar not null,
    home_score smallint not null,
    away_team varchar not null,
    away_score smallint not null,
    foreign key (home_team) REFERENCES team(team_id),
    foreign key (away_team) REFERENCES team(team_id),
    PRIMARY KEY (gsis_id)
);

create table drive(
    gsis_id integer not null,
    drive_id smallint not null,
    start_field integer null,
    start_time integer not null,
    end_field integer null,
    end_time integer not null,
    pos_team varchar not null,
    pos_time integer null,
    foreign key (gsis_id) REFERENCES game(gsis_id),
    foreign key (pos_team) REFERENCES team(team_id),
    PRIMARY KEY (gsis_id, drive_id)

);

create table play(
    gsis_id integer not null,
    drive_id smallint not null,
    play_id smallint not null,
    time integer not null,
    pos_team varchar not null,
    yardline integer null,
    down smallint null,
    yards_to_go smallint null,
    foreign key (pos_team) REFERENCES team(team_id),
    foreign key (gsis_id) REFERENCES game(gsis_id),
    foreign key (drive_id) REFERENCES drive(drive_id),
    PRIMARY KEY (gsis_id, drive_id, play_id)
);

create table play_player(
    gsis_id integer not null,
    drive_id smallint not null,
    play_id smallint not null,
    player_id varchar not null,
    team varchar not null,
    foreign key (team) REFERENCES team(team_id),
    foreign key (gsis_id) REFERENCES game(gsis_id),
    foreign key (drive_id) REFERENCES drive(drive_id),
    foreign key (play_id) REFERENCES play(play_id),
    foreign key (player_id) REFERENCES player(player_id),
    PRIMARY KEY (gsis_id, drive_id, play_id, player_id)
) ;

create table meta(
    version smallint null,
    season_type integer null,
    season_year smallint null,
    week smallint null
);
